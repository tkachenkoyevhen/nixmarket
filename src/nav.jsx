import React from 'react'
import { NavigationContainer } from '@react-navigation/native'

import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { useSelector } from 'react-redux'

import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import NavProducts from './pages/product/nav.product'
import Basket from './pages/basket'

const Tab = createBottomTabNavigator()

export default function Nav () {
  const count = useSelector((state) => state.basket.count)
  
  return (
    <NavigationContainer>
      <Tab.Navigator initialRouteName="Products" screenOptions={{ headerShown: false }}>
        <Tab.Screen name="Products" component={NavProducts} options={{
            tabBarIcon: ({color, size}) => (
              <MaterialCommunityIcons name="storefront" color={color} size={size} />
            )
          }} />
        <Tab.Screen name="Basket" component={Basket} options={{
          tabBarIcon: ({color, size}) => (
            <MaterialCommunityIcons name="basket" color={color} size={size} />
          ),
          tabBarBadge: count || null
        }} />
      </Tab.Navigator>
    </NavigationContainer>
  );
}