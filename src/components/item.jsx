import React from 'react'
import { StyleSheet, View, Image, Text } from 'react-native';
import { Button } from 'react-native';
import { useDispatch, useSelector } from 'react-redux'

import { _create, _delete } from '../store/modules/basket'
import { calculateTotalCost } from '../store/modules/basket'

export default function Item ({item}) {
  const { id, title, img, price, colour } = item
  const _colour = colour.toLowerCase()
  const basket = useSelector((state) => state.basket)
  const dispatch = useDispatch()

  const navToDetail = () => {
    navigation.navigate('Details')
  }

  const addToBasket = () => {
    dispatch(_create.bind(item))
    dispatch(calculateTotalCost)
  }

  const removeFromBasket = () => {
    dispatch(_delete.bind(item))
    dispatch(calculateTotalCost)
  }
  
  const isProductInBasket = () => {
    const {items} = basket
    return !items.filter((el) => el.id === id)?.length
  }

  return (
    <View style={styles.item} onClick={navToDetail} key={id}>
      <View style={styles.picture}>
        <Image style={styles.image} source={{ uri: img }} />
      </View>
      <View style={styles.info}>
        <Text style={styles.title}>{title}</Text>
        <Text style={styles.price}>Cost: {price}$</Text>
        <Text>Color: <View style={[styles.color, {backgroundColor: _colour}]}></View></Text>
      </View>
      <View style={styles.button}>
        {isProductInBasket() && <Button title='Add' onPress={addToBasket} />}
        {!isProductInBasket() && <Button title='Remove' onPress={removeFromBasket} />}
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  item: {
    backgroundColor: '#f9c2ff',
    padding: 5,
    marginVertical: 8,
    marginHorizontal: 16,
    display: 'flex',
    flexDirection: 'row',
  },
  info: {
    display: 'flex',
    flexShrink: 'initial',
    paddingLeft: 10,
    flexGrow: 1
  },
  title: {
    fontSize: 14,
  },
  picture: {
    justifyContent: 'center',
  },
  button: {
    justifyContent: 'center',
  },
  image: {
    width: 50,
    height: 50,
  },
  price: {
    paddingTop: 2,
  },
  color: {
    width: 10,
    height: 10,
  }
});
