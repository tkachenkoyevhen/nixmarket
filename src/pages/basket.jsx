import React from 'react'
import { View, SafeAreaView, FlatList, StatusBar, StyleSheet, Text } from 'react-native'
import { useSelector } from 'react-redux'

import Item from '../components/item'

const renderItem = ({ item }) => (
  <Item item={item} />
);

export default function Basket () {
  const basket = useSelector((state) => state.basket)
  const total = useSelector((state) => state.basket.totalCost)

  const notEmpty = () => !!basket.items.length

  return (
    <View style={styles.container}>
      <SafeAreaView>
        { notEmpty() && <FlatList data={basket.items} renderItem={renderItem} keyExtractor={item => item.id} />}
        { notEmpty() && <Text>Total cost: {total}$</Text> }
        { !notEmpty() && <Text style={styles.empty}>The basket is empty!</Text> }
      </SafeAreaView>
      <StatusBar style="auto" />
  </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: StatusBar.currentHeight || 0,
  },
  empty: {
    alignSelf: 'center'
  }
});
