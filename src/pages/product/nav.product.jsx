import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack'

import Products from './products';
import ProductDetails from './productDetail';

const ProductStack = createNativeStackNavigator();

export default function NavProducts () {
  return (
    <ProductStack.Navigator>
      <ProductStack.Screen name="Product" component={Products} />
      <ProductStack.Screen name="Details" component={ProductDetails} />
    </ProductStack.Navigator>
  );
}
