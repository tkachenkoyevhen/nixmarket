import React, { useEffect } from 'react'
import { View, StatusBar, StyleSheet, FlatList, SafeAreaView, Image } from 'react-native'
import { useSelector, useDispatch } from 'react-redux'
import { fetchProducts } from '../../store/modules/product'

import Item from '../../components/item'

const renderItem = ({ item }) => (
  <Item item={item} />
);

export default function Products () {
  const products = useSelector(state => state.products)
  const dispatch = useDispatch()

  const getProducts = () => {
    dispatch(fetchProducts)
  }
  
  useEffect(() => {
    getProducts()
  }, [])

  return (
    <View>
      <SafeAreaView style={styles.container}>
        <FlatList data={products} renderItem={renderItem} keyExtractor={item => item.id} />
      </SafeAreaView>
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: StatusBar.currentHeight || 0,
  },
});
