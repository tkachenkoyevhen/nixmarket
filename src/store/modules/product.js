const HOST = 'https://my-json-server.typicode.com'
const GET_PRODUCTS = 'GET_PRODUCTS'

const initialState = []

function products (state = initialState, actions) {
  switch (actions.type) {
    case GET_PRODUCTS:
      return actions.payload
    default:
      return state;
  }
}

export async function fetchProducts (dispatch, getState) {
  const response = await fetch(`${HOST}/benirvingplt/products/products`, { method: 'GET' })
  const payload = await response.json()
  dispatch({ type: GET_PRODUCTS, payload: payload.map(el => ({ id: el.id, title: el.name, ...el }) ) })
}

export default products
