const ADD_PRODUCT = 'ADD_PRODUCT'
const DELETE_PRODUCT = 'DELETE_PRODUCT'
const GET_TOTAL_COST = 'GET_TOTAL_COST'

const initialState = {
  items: [],
  count: 0,
  totalCost: 0
}

function basket (state = initialState, actions) {
  switch (actions.type) {
    case ADD_PRODUCT:
      return {
        ...state,
        items: [...state?.items, actions.payload],
        count: state.count + 1,
      }
    case DELETE_PRODUCT:
      return {
        ...state,
        items: (state.items.length)
          ? state.items.filter((el) => el.id !== actions.payload.id)
          : [],
        count: state.count - 1,
        state
      }
    case GET_TOTAL_COST:
      return {
        ...state,
        totalCost: (state.items.length)
          ? state.items.map(el => parseFloat(el.price))
          .reduce((prev, curr) => prev + curr)
          .toFixed(2)
          : 0
      }
    default:
      return state;
  }
}

export function _create (dispatch) {
  dispatch({
    type: ADD_PRODUCT,
    payload: this,
  })
}

export function _delete (dispatch) {
  dispatch({
    type: DELETE_PRODUCT,
    payload: this,
  })
}

export function calculateTotalCost(dispatch) {
  dispatch({
    type: GET_TOTAL_COST,
  })
}

export default basket
