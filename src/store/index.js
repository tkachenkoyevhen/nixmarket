import { createStore, applyMiddleware, combineReducers } from 'redux';
import thunkMiddleware from 'redux-thunk'
import { composeWithDevTools } from 'redux-devtools-extension'
import basket from './modules/basket';
import products from './modules/product';

const composedEnhancer = composeWithDevTools(applyMiddleware(thunkMiddleware))

const Modules = combineReducers({
  basket,
  products,
})

const store = createStore(Modules, composedEnhancer);

export default store
