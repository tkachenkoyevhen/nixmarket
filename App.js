import React from 'react';
import { Provider } from 'react-redux';
import store from './src/store/index'

import Nav from './src/nav';

export default function App() {
  return (
    <Provider store={store}>
      <Nav />
    </Provider>
  );
}
